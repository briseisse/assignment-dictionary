; In this file i will create the find_word function.
; find_word function will take two parameters:
; pointer to the null terminated strong and pointer to the begining
; the dictionary

%define SIZE 8
section .text
global find_word

extern string_equals
extern string_length

find_word:
    xor rax, rax
    mov r9, rsi
    mov r8, rdi
    .loop:
        add r9, SIZE
        mov rdi, r8
        mov rsi, r9
        push r9
        push r8
        call string_equals
        pop r8
        pop r9
        cmp rax, 1
        je .success
        mov r9, [re - SIZE]
        cmp r9, 0
        je .failure
        jmp .loop

    .success:               ; In case the key is found.
        sub r9, SIZE
        mov rax, r9
        ret

    .failure:               ; Otherwhise
        xor rax, rax
        ret


