; Файл words.inc должен хранить слова, определённые с помощью макроса colon.
; Включите этот файл в main.asm.

%include "colon.inc"

colon "this", this
db "this_value",0

colon "key4", fourth
db "key4_value",0

colon "key3", third
db "key3_value", 0

colon "key2", second
db "key2_value",0

colon "key1", first
db "key1_value",0
