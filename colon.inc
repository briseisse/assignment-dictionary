; File colon in wich i will keep the words.
; I define the macro for word creation in the dictionary.
; This macros will take two parameters: the key(first parm) and 
; the value(second parm).

%define NEXT_ELEMENT 0
%macro colon 2


    %ifstr %1
        db %1, 0
    %else
        %fatal "param1: The key should be a string!"
    %endif
    
    %ifid %2
        %2: dq NEXT_ELEMENT
        %define NEXT_ELEMENT %2
    %else
        %fatal "param2: The value is needed!"
    %endif

%endmacro
