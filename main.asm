; В main.asm определите функцию _start, которая:
; Читает строку размером не более 255 символов в буфер с stdin.
; Пытается найти вхождение в словаре; если оно найдено, распечатывает в stdout значение по этому ключу.
; Иначе выдает сообщение об ошибке.
; Не забудьте, что сообщения об ошибках нужно выводить в stderr.

%include "lib.inc"
%include "words.inc"
%define BUFFER 256
%define DQ_SIZE 8

extern find_word

global _start

section .data

failure: db "Key not found!", 0
string_buffer: times BUFFER db 0
overflow: db "Buffer overflow", 0

section .text

; First of all, enter the bufer
_start:
    xor rax, rax
    mov rdi, string_buffer
    mov rsi, BUFFER
    call read_word
    test rax, rax
    jne .success_read_buffer    ; if (overflow){rax return 0 }
    mov rdi, overflow
    call print_err
    call print_newline
    call exit

;
.success_read_buffer:
    mov rdi, rax
    mov rsi, first
    push rdx                    ; line length save
    call find_word
    test rax, rax               ; if key not found, rax = 0
    jne .success                ; in case the key is found
    mov rdi, failure            ; otherwhise
    call print_err
    call print_newline
    call exit

.success:
    pop rdx                     ; return line length
    add rax, 8                  ; next element
    add rax, rdx
    add rax, 1
    mov rdi, rax                ; found line(shown by rax)
    call print_string
    call print_newline
    call exit

print_err:
    xor rax, rax
    mov rsi, rdi
    call string_length
    mov rdx, rax
    mov rdi, 2
    mov rax, 1
    syscall
    ret
